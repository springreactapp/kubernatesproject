package com.incedo.rhpam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author SUNIL
 *
 */
@SpringBootApplication
//@EnableEurekaClient
public class SpringBootEureakaUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEureakaUserApplication.class, args);
		
		
		
	}

}
