package com.incedo.rhpam.controller;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.incedo.rhpam.dto.Response;
import com.incedo.rhpam.entity.Candidate;
import com.incedo.rhpam.parse.CommonParser;
import com.incedo.rhpam.service.CandidateService;
import com.incedo.rhpam.validation.CandidateRequest;


/**
 * @author SUNIL
 *
 */
@RestController
@RequestMapping("/candidate")
public class CandidateController {
	@Autowired
	private CandidateService candidateService;
	@Autowired
	private CommonParser commonParser;
	
	@RequestMapping(name="register",method=RequestMethod.POST,consumes={"application/JSON","application/XML"},produces={"application/JSON","application/XML"})
	public 	Response registerCandidate( @RequestBody CandidateRequest candidateRequest){
		Response response =new Response();
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<CandidateRequest>> constraintViolations = validator.validate(candidateRequest);
		if(constraintViolations.size()==0){
		Candidate candidate= commonParser.getCandidate(candidateRequest);
		candidateService.saveCandidateDetails(candidate);
			response.setMessage("user Registraction sucessfully");
			response.setErrorCode("200");
			response.setStatus("sucess");
		}else{
			 for (ConstraintViolation<CandidateRequest> violation : constraintViolations) {
				 response.setMessage(violation.getMessage());
				 response.setErrorCode("500");
				 response.setStatus("failure");
		}
	}
		
		return response;
	}
}
