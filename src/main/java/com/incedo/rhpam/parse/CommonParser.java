package com.incedo.rhpam.parse;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.incedo.rhpam.entity.Candidate;
import com.incedo.rhpam.entity.User;
import com.incedo.rhpam.validation.CandidateRequest;
import com.incedo.rhpam.validation.UserRequest;

@Component
public class CommonParser {

	public User getUser(UserRequest userrequest){
		User user =new User();
		user.setFirstName(userrequest.getFirstName());
		user.setLastName(userrequest.getLastName());
		user.setGender(userrequest.getGender().toString());
		//user.setRoles(userrequest.getRoles().toString());
		//user.setStatus(userrequest.getStatus().toString());
		return user;
		
	}
	
	
	public Candidate getCandidate(CandidateRequest candidateRequest){
		Candidate candidate = new Candidate();
		candidate.setCandidate_fname(candidateRequest.getFirstName());
		candidate.setCandidate_lname(candidateRequest.getLastName());
		candidate.setCandidateEmail(candidateRequest.getCandidateemail());
		candidate.setCandidateNumber(candidateRequest.getMobileNumber());
		candidate.setCandaiateExp(candidateRequest.getCandidateExp());
		candidate.setCandidateAddress1(candidateRequest.getCandidateaddress1());
		candidate.setCandidateAddress2(candidateRequest.getCandidateaddress2());
		candidate.setCandidateAddress3(candidateRequest.getCandidateaddress3());
		candidate.setCandidateAddress4(candidateRequest.getCandidateaddress4());
		candidate.setCandidateCity(candidateRequest.getCandidateCity());
		candidate.setCandidateCountry(candidateRequest.getCandidateCountry());
		candidate.setCandidateCreatedDt(new Date());
		candidate.setCandidatePin(candidateRequest.getPin());
		candidate.setCandidateModifiedDate(new Date());
		return candidate;
		
	}
}
