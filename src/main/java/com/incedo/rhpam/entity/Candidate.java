package com.incedo.rhpam.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author SUNIL
 *
 */
@Entity
@Table(name="candidate_details")
public class Candidate implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7049484616027643526L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="candidate_id")
	private int candidate_id;
	@Column(name="candidate_fname")
	private String candidate_fname;  
	@Column(name="candidate_lname")
	private String candidate_lname; 
	@Column(name="candidate_email")
	private String candidateEmail;
	
	@Column(name="candidate_mobile")
	private String candidateNumber; 
	@Column(name="candidate_exp")
	private String candaiateExp;  
	@Column(name="candidate_address1")
	private String candidateAddress1;  
	@Column(name="candidate_address2")
	private String candidateAddress2;  
	@Column(name="candidate_address3")
	private String candidateAddress3; 
	@Column(name="candidate_address4")
	private String candidateAddress4; 
	@Column(name="candidate_city")
	private String candidateCity; 
	@Column(name="candidate_country")
	private String candidateCountry; 
	@Column(name="candidate_pin")
	private Integer candidatePin;  
	@Column(name="candidate_created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date candidateCreatedDt;  
	@Column(name="candidate_last_modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date candidateModifiedDate;
	
	@OneToMany(mappedBy="candidate")
	private Set<SkillDetails> skills;
	
	
	public int getCandidate_id() {
		return candidate_id;
	}
	public void setCandidate_id(int candidate_id) {
		this.candidate_id = candidate_id;
	}
	public String getCandidate_fname() {
		return candidate_fname;
	}
	public void setCandidate_fname(String candidate_fname) {
		this.candidate_fname = candidate_fname;
	}
	public String getCandidate_lname() {
		return candidate_lname;
	}
	public void setCandidate_lname(String candidate_lname) {
		this.candidate_lname = candidate_lname;
	}
	
	public String getCandidateNumber() {
		return candidateNumber;
	}
	public void setCandidateNumber(String candidateNumber) {
		this.candidateNumber = candidateNumber;
	}
	public String getCandaiateExp() {
		return candaiateExp;
	}
	public void setCandaiateExp(String candaiateExp) {
		this.candaiateExp = candaiateExp;
	}
	public String getCandidateAddress1() {
		return candidateAddress1;
	}
	public void setCandidateAddress1(String candidateAddress1) {
		this.candidateAddress1 = candidateAddress1;
	}
	public String getCandidateAddress2() {
		return candidateAddress2;
	}
	public void setCandidateAddress2(String candidateAddress2) {
		this.candidateAddress2 = candidateAddress2;
	}
	public String getCandidateAddress3() {
		return candidateAddress3;
	}
	public void setCandidateAddress3(String candidateAddress3) {
		this.candidateAddress3 = candidateAddress3;
	}
	public String getCandidateAddress4() {
		return candidateAddress4;
	}
	public void setCandidateAddress4(String candidateAddress4) {
		this.candidateAddress4 = candidateAddress4;
	}
	public String getCandidateCity() {
		return candidateCity;
	}
	public void setCandidateCity(String candidateCity) {
		this.candidateCity = candidateCity;
	}
	public String getCandidateCountry() {
		return candidateCountry;
	}
	public void setCandidateCountry(String candidateCountry) {
		this.candidateCountry = candidateCountry;
	}
	public Integer getCandidatePin() {
		return candidatePin;
	}
	public void setCandidatePin(Integer candidatePin) {
		this.candidatePin = candidatePin;
	}
	public Date getCandidateCreatedDt() {
		return candidateCreatedDt;
	}
	public void setCandidateCreatedDt(Date candidateCreatedDt) {
		this.candidateCreatedDt = candidateCreatedDt;
	}
	public Date getCandidateModifiedDate() {
		return candidateModifiedDate;
	}
	public void setCandidateModifiedDate(Date candidateModifiedDate) {
		this.candidateModifiedDate = candidateModifiedDate;
	}
	public String getCandidateEmail() {
		return candidateEmail;
	}
	public void setCandidateEmail(String candidateEmail) {
		this.candidateEmail = candidateEmail;
	}
	public Set<SkillDetails> getSkills() {
		return skills;
	}
	public void setSkills(Set<SkillDetails> skills) {
		this.skills = skills;
	}  






}
