package com.incedo.rhpam.validation;

public class PreviousCompanyDetails {

	private String previousCompanyName;
	private String previousCompanyAddress1;
	private String previousCompanyAddress2;
	private String previousCompanyAddress3;
	private String previousCompanyCity;
	private String previousCompanyState;
	private String previousComapnyCountry;
	private String previousCompanyPinCode;
	
	
	public String getPreviousCompanyName() {
		return previousCompanyName;
	}
	public void setPreviousCompanyName(String previousCompanyName) {
		this.previousCompanyName = previousCompanyName;
	}
	public String getPreviousCompanyAddress1() {
		return previousCompanyAddress1;
	}
	public void setPreviousCompanyAddress1(String previousCompanyAddress1) {
		this.previousCompanyAddress1 = previousCompanyAddress1;
	}
	public String getPreviousCompanyAddress2() {
		return previousCompanyAddress2;
	}
	public void setPreviousCompanyAddress2(String previousCompanyAddress2) {
		this.previousCompanyAddress2 = previousCompanyAddress2;
	}
	public String getPreviousCompanyAddress3() {
		return previousCompanyAddress3;
	}
	public void setPreviousCompanyAddress3(String previousCompanyAddress3) {
		this.previousCompanyAddress3 = previousCompanyAddress3;
	}
	public String getPreviousCompanyCity() {
		return previousCompanyCity;
	}
	public void setPreviousCompanyCity(String previousCompanyCity) {
		this.previousCompanyCity = previousCompanyCity;
	}
	public String getPreviousCompanyState() {
		return previousCompanyState;
	}
	public void setPreviousCompanyState(String previousCompanyState) {
		this.previousCompanyState = previousCompanyState;
	}
	public String getPreviousComapnyCountry() {
		return previousComapnyCountry;
	}
	public void setPreviousComapnyCountry(String previousComapnyCountry) {
		this.previousComapnyCountry = previousComapnyCountry;
	}
	public String getPreviousCompanyPinCode() {
		return previousCompanyPinCode;
	}
	public void setPreviousCompanyPinCode(String previousCompanyPinCode) {
		this.previousCompanyPinCode = previousCompanyPinCode;
	}
	
}
