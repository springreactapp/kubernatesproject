package com.incedo.rhpam.validation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NotNull(message = "candidate skills Required")
public class Skill {

	@NotNull(message = "SkillName cannot be null")
	@Size(min = 3, max = 50, message 
	= "skill Name minimum 3 letters")
	private String skillName;
	@NotNull(message = "ExpSkill cannot be null")

	private String expSkill;
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public String getExpSkill() {
		return expSkill;
	}
	public void setExpSkill(String expSkill) {
		this.expSkill = expSkill;
	}

}
