package com.incedo.rhpam.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.incedo.rhpam.entity.Candidate;
import com.incedo.rhpam.entity.User;

/**
 * @author SUNIL
 *
 */

@Repository
public class CandidateDaoImpl implements CandidateDao{
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public CandidateDaoImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/*@Override
	public int saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		return (int) session.save(user);
	}*/

	@Override
	public int saveCandidate(Candidate candidate) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		return (int)session.save(candidate);
	}

	/*@Override
	public void deleteUserById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		 Query query=session.getNamedSQLQuery("deleteUserById");
		 query.setParameter("id", id);
		
	}*/
	
}
